package com.cloudcomputing.smartmemento.login;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by yaminikancharana on 5/5/16.
 */
public interface createUserInterface {
    @POST("/api/Account/Register")
    Call<Response> createUser(@Body Map<String, String> loginInfo);

}
