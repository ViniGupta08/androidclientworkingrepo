package com.cloudcomputing.smartmemento.login;

/**
 * Created by CUCS\aa2355 on 3/28/16.
 */
public class Response {

    public int statusCode;
    public boolean success;
    public String message;
    public Object obj;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return obj;
    }

    public void setObject(Object object) {
        this.obj = object;
    }

    @Override
    public String toString() {
        return "Response{" +
                "statusCode=" + statusCode +
                ", success=" + success +
                ", message='" + message + '\'' +
                ", object=" + obj +
                '}';
    }
}
