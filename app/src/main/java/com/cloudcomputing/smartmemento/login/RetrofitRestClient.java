package com.cloudcomputing.smartmemento.login;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by CUCS\aa2355 on 3/28/16.
 */
public class RetrofitRestClient {

    private String baseUrl;
    private Retrofit.Builder retrofitBuilder;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public RetrofitRestClient(String baseUrl) {
        this.baseUrl = baseUrl;
        this.retrofitBuilder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create());
    }

    public Retrofit.Builder getBuilder() {
        return this.retrofitBuilder;
    }

    public <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

}
