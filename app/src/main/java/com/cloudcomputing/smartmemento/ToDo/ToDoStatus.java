package com.cloudcomputing.smartmemento.ToDo;

public enum ToDoStatus {
    PENDING,
    COMPLETED
}