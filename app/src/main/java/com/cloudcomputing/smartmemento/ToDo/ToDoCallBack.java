package com.cloudcomputing.smartmemento.ToDo;

import com.cloudcomputing.smartmemento.googleplaces.Place;

import java.util.List;

/**
 * Created by vinig on 03/27/16.
 */
public interface ToDoCallBack {
    public void onCompleted(List<ToDo> ToDoList);
}