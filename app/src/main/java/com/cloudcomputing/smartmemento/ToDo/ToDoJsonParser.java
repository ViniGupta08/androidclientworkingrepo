package com.cloudcomputing.smartmemento.ToDo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ToDoJsonParser {
    public List<ToDo> parse(JSONObject jsonObject) {
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONObject("obj").getJSONArray("pendingTasks");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getToDos(jsonArray);
    }

    private List<ToDo> getToDos(JSONArray jsonArray) {
        int ToDoCount = jsonArray.length();
        List<ToDo> toDoList = new ArrayList<ToDo>();
        for (int i = 0; i < ToDoCount; i++) {
            try {
                ToDo todo = getToDo((JSONObject) jsonArray.get(i));
                if(todo != null) {
                    toDoList.add(todo);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return toDoList;
    }

    private ToDo getToDo(JSONObject ToDoJson) {
        ToDo todo = null;
        try {
            if (!ToDoJson.isNull("taskId")) {
                todo = new ToDo();
                todo.Id = Integer.parseInt(ToDoJson.getString("taskId"));
                todo.Title = ToDoJson.getString("taskName");
                todo.Description = ToDoJson.getString("taskDescription");
                todo.StartTime = ToDoJson.getString("taskEntry");
                todo.EndTime = ToDoJson.getString("taskDeadline");
                JSONObject categoryObject = ToDoJson.getJSONObject("mainCategory");
                todo.Category = categoryObject.getString("categoryName");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return todo;
    }
}