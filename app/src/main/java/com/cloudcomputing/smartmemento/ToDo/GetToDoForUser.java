package com.cloudcomputing.smartmemento.ToDo;

import java.util.*;
import java.util.SimpleTimeZone;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by yaminikancharana on 5/5/16.
 */

//when the user logs in, get the ToDo List data against the user from the backend. if the user doesn't exist create one
    //and assign and empty todo list

public interface GetToDoForUser {

    @POST("/api/user/adduser") //type of request and specify url we are going to consume from
    Call<ArrayList<ToDo>> getToDoForUser(@Body Map<String,String> map);
}
