package com.cloudcomputing.smartmemento.ToDo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by yaminikancharana on 5/5/16.
 */
public interface UpdToDoInterface {

    @POST("/getTodoList") //incomplete, add api path, take in username also as parameter
    Call<ToDo> updToDoList(@Body ToDo toDo);

}
