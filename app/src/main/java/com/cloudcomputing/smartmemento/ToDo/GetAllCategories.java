package com.cloudcomputing.smartmemento.ToDo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by yaminikancharana on 5/5/16.
 */
public interface GetAllCategories {
    @GET("/getTodoList") //type of request and specify url we are going to consume from
    Call<ArrayList<ToDo>> getRestToDoList();
}
