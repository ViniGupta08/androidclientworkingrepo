package com.cloudcomputing.smartmemento.ToDo;

import com.cloudcomputing.smartmemento.login.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by yaminikancharana on 5/3/16.
 */

//add one new task to todo. Take username, toDo object as input
public interface AddToToDoInterface {

    @POST("/api/Task/create/{username}") //type of request and specify url we are going to consume from
    Call<Response> addToRestToDoList(@Path("username") String mEmail, @Body Map<String,String> atdMap);
}
