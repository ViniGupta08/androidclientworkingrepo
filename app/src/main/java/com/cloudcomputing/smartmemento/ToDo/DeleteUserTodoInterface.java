package com.cloudcomputing.smartmemento.ToDo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;

/**
 * Created by yaminikancharana on 5/5/16.
 */
public interface DeleteUserTodoInterface {

    @DELETE("/api/Values?id = email") //type of request and specify url we are going to consume from
    Call<ToDo> deleteTodo(String email);
}
