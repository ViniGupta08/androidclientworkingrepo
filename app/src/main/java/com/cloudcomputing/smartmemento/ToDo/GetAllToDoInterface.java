package com.cloudcomputing.smartmemento.ToDo;

import com.cloudcomputing.smartmemento.login.Response;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by yaminikancharana on 5/3/16.
 */
public interface GetAllToDoInterface {
    @GET("/api/user/getuser/{username}") //type of request and specify url we are going to consume from
    Call<Response> getRestToDoList(@Path("username") String uName);
}


