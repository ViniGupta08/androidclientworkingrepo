package com.cloudcomputing.smartmemento.ToDo;

import java.io.Serializable;
import java.util.Date;

public class ToDo implements Serializable {
    public int Id;
    public String Title;

    public String Category;

    public String Description;

    public String StartTime;

    public String EndTime;

    public ToDoStatus Status;

    public ToDo(){
        this.Id = -1;
        Title = "";
        Description = "";
        this.Category = "";
        StartTime = "";
        EndTime = "";
        this.Status = ToDoStatus.PENDING;
    }

    public ToDo(int id, String title, String description, String category, String startTime, String endTime){
        this.Id = id;
        this.Title = title;
        this.Description = description;
        this.Category = category;
        this.StartTime = startTime;
        this.EndTime = endTime;
        this.Status = ToDoStatus.PENDING;
    }

    public ToDo(int id, String title, String description, String category, Date startTime, Date endTime){
        this.Id = id;
        this.Title = title;
        this.Description = description;
        this.StartTime = startTime.toString();
        this.EndTime = endTime.toString();
        this.Category = category;
        this.Status = ToDoStatus.PENDING;
    }

    public void update(String title, String description, String startTime, String endTime,ToDoStatus status){
        this.Title = title;
        this.Description = description;
        this.StartTime = startTime;
        this.EndTime = endTime;
        this.Status = status;
    }

    public String getJSON(){
        /* change variable name as per the service name*/
        /*create a function which takes an object and return its json*/
        String JsonDATA = "{\"taskId\" : " + this.Id + ", \"taskName\" : \"" + this.Title + "\", \"taskDescription\" : \"" + this.Description + "\",\n" +
                "    \"taskEntry\" : " + this.StartTime + ",\n" +
                "    \"taskEnd\" : " + this.EndTime + ",\n" +
                "    \"mainCategory\": {\n" +
                "      \"categoryId\" : " + this.Category + "\n" +
                "    },\n" +
                "}";
        return JsonDATA;
    }
}
