package com.cloudcomputing.smartmemento.ToDo;

import android.os.AsyncTask;
import android.util.Log;

import com.cloudcomputing.smartmemento.LoginActivity;
import com.cloudcomputing.smartmemento.utilities.HttpFetcher;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ToDoFetcher extends AsyncTask<Object, Integer, String> {
    String toDoData = null;
    ToDoCallBack toDoCallBack;
    public ToDoFetcher(ToDoCallBack toDoCallBack){
        this.toDoCallBack = toDoCallBack;
    }
    @Override
    protected String doInBackground(Object... inputObj) {
        try {
            String getTodoForUserUrl = (String)inputObj[0];
            HttpFetcher http = new HttpFetcher();
            toDoData = http.read(getTodoForUserUrl);
        } catch (Exception e) {
            Log.d("User Todo Read Task", e.toString());
        }
        return toDoData;
    }

    @Override
    protected void onPostExecute(String result) {
        List<ToDo> toDoList = null;
        ToDoJsonParser todoJsonParser = new ToDoJsonParser();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(result);
            toDoList=todoJsonParser.parse(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        toDoCallBack.onCompleted(toDoList);
    }
}