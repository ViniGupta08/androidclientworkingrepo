package com.cloudcomputing.smartmemento.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cloudcomputing.smartmemento.Categories.Category;
import com.cloudcomputing.smartmemento.ToDo.ToDo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class DatabaseHandler extends SQLiteOpenHelper {
// All Static variables
    public static final String LOG = "myLogs";
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "toDoManager";

    // Contacts table name
    private static final String TABLE_TODO = "ToDoTable";
    private static final String TABLE_CATEGORY = "CategoryTable";

    // To_Do Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_START_TIME = "start_time";
    private static final String KEY_END_TIME = "end_time";
    private static final String KEY_STATUS = "status";

    private static final String TABLE_CATEGORY_ID = "categoryId";
    private static final String TABLE_CATEGORY_NAME = "categoryName";
    private static final String TABLE_CATEGORY_DESCRIPTION = "categoryDescription";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TODO_TABLE = "CREATE TABLE " + TABLE_TODO + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_DESCRIPTION + " TEXT," + KEY_CATEGORY + " TEXT," + KEY_START_TIME + " TEXT," + KEY_END_TIME + " TEXT," + KEY_STATUS + " TEXT" + ")";
        String CREATE_Category_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "("
                + TABLE_CATEGORY_ID + " TEXT," + TABLE_CATEGORY_NAME + " TEXT,"
                + TABLE_CATEGORY_DESCRIPTION + " TEXT" + ")";
	db.execSQL(CREATE_Category_TABLE);
	db.execSQL(CREATE_TODO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        // Create tables again
        onCreate(db);
    }

    public void addToDo(ToDo toDo){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, toDo.Id);
        values.put(KEY_TITLE, toDo.Title);
        values.put(KEY_DESCRIPTION, toDo.Description);
        values.put(KEY_CATEGORY, toDo.Category.toLowerCase());
        values.put(KEY_START_TIME, toDo.StartTime);
        values.put(KEY_END_TIME, toDo.EndTime);
        values.put(KEY_STATUS, toDo.Status.toString());

        // Inserting Row
        db.insert(TABLE_TODO, null, values);
        db.close(); // Closing database connection
    }

    public ToDo getToDo(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TODO, new String[] { KEY_ID, KEY_TITLE,
                        KEY_DESCRIPTION, KEY_CATEGORY, KEY_START_TIME, KEY_END_TIME, KEY_STATUS }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ToDo toDo = new ToDo(Integer.parseInt(cursor.getString(0)), cursor.getString(1),cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
        // return todo
        return toDo;
    }

    public ArrayList<ToDo> getAllToDo() {
        ArrayList<ToDo> toDoList = new ArrayList<ToDo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TODO;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
                ToDo toDo = new ToDo(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));

                toDoList.add(toDo);
            } while (cursor.moveToNext());
        }

        // return todo list
        return toDoList;
    }

    public int getToDoCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TODO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count =0;
        if(cursor != null && !cursor.isClosed()){
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

        public boolean DoesCategoryTableExists() {
            String tableName=TABLE_CATEGORY;
            SQLiteDatabase mDatabase = this.getReadableDatabase();
            if(mDatabase == null || !mDatabase.isOpen()) {
                mDatabase = getReadableDatabase();
            }

            if(!mDatabase.isReadOnly()) {
                mDatabase.close();
                mDatabase = getReadableDatabase();
            }
            Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
            if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public void insertCategory(ArrayList<Category> categories){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CATEGORY, null, null);
        for(Category category: categories) {
            ContentValues values = new ContentValues();
            values.put(TABLE_CATEGORY_ID, category.categoryId);
            values.put(TABLE_CATEGORY_NAME, category.categoryName);
            values.put(TABLE_CATEGORY_DESCRIPTION, category.categoryDescription);
            // Inserting Row
            db.insert(TABLE_CATEGORY, null, values);
        }
        db.close(); // Closing database connection
    }
    public int getCategoryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CATEGORY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count =0;
        if(cursor != null && !cursor.isClosed()){
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }
    /*public String getExpiryTime(String tableName) {
        String expiryTime=DateFormat.getDateTimeInstance().format("MM/dd/yyyy hh:mm:ss aa");
        //String expiryTimeQuery = "SELECT  * FROM " + TABLE_METADATA + "where " + TABLE_METADATA_TABLENAME + "=" + tableName;
        Log.e(LOG, expiryTimeQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(expiryTimeQuery, null);

        if(cursor != null && !cursor.isClosed()) {
            expiryTime = cursor.getString(cursor.getColumnIndex(TABLE_METADATA_EXPIRYTIME));
            cursor.close();
        }
        return expiryTime;

    } */
        public ArrayList<String> getAllCategoriesNameInDB(){
            ArrayList<String> categories = new ArrayList<String>();
            String selectQuery = "SELECT DISTINCT "+KEY_CATEGORY +" FROM " + TABLE_TODO;
            Log.e(LOG, selectQuery);

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (c.moveToFirst()) {
                do {
                    String categoryName = c.getString(c.getColumnIndex(KEY_CATEGORY));
                    categories.add(categoryName.toLowerCase());
                } while (c.moveToNext());
            }
            return categories;
        }
       public List<Category> getAllCategoriesFromDB(){

        List<Category> categories = new ArrayList<Category>();
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
                Cursor c = db.rawQuery(selectQuery, null);

                // looping through all rows and adding to list
                if (c.moveToFirst()) {
                    do {
                Category category = new Category();
                category.categoryId = c.getString(c.getColumnIndex(TABLE_CATEGORY_ID));
                category.categoryName = c.getString(c.getColumnIndex(TABLE_CATEGORY_NAME));
                category.categoryDescription = c.getString(c.getColumnIndex(TABLE_CATEGORY_DESCRIPTION));
                categories.add(category);
            } while (c.moveToNext());
        }
        return categories;
    }
    public int nextId(){
        return this.getToDoCount() + 1;
    }

    public int updateToDo(ToDo toDo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, toDo.Title);
        values.put(KEY_DESCRIPTION, toDo.Description);
        values.put(KEY_CATEGORY, toDo.Category);
        values.put(KEY_START_TIME, toDo.StartTime);
        values.put(KEY_END_TIME, toDo.EndTime);
        values.put(KEY_STATUS, toDo.Status.toString());

        // updating row
        int count = db.update(TABLE_TODO, values, KEY_ID + " = ?",
                new String[]{String.valueOf(toDo.Id)});
        db.close(); // Closing database connection
        return count;
    }

    public void deleteToDo(ToDo toDo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TODO, KEY_ID + " = ?",
                new String[] { String.valueOf(toDo.Id) });
        db.close();
    }
}
