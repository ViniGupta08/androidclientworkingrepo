package com.cloudcomputing.smartmemento.DB;

import java.sql.Timestamp;

/**
 * Created by vinig on 03/25/16.
 */
public class MetadataTable {
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Timestamp getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Timestamp expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    String tableName;
    Timestamp expiryTime;
    String versionNumber;
}
