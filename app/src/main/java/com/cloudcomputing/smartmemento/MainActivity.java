package com.cloudcomputing.smartmemento;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudcomputing.smartmemento.Categories.CategoriesCustomAdapter;
import com.cloudcomputing.smartmemento.Categories.Category;
import com.cloudcomputing.smartmemento.DB.DatabaseHandler;
import com.cloudcomputing.smartmemento.googleplaces.PlacesCallBack;
import com.cloudcomputing.smartmemento.utilities.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.cloudcomputing.smartmemento.ToDo.ToDo;
import com.cloudcomputing.smartmemento.googleplaces.*;
import com.cloudcomputing.smartmemento.utilities.GeocoderHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PlacesCallBack {

    GoogleMap mMap;
    EditText searchQueryET;
    TextView userNameET;
    TextView emailIDET;

    protected TextView mLocationText;
    Context context;
    Toolbar toolbar;

    double latitude;
    double longitude;

    //Location Listener
    LocationManager locationManager;
    LocationListener locationListener;

    final int PROXIMITY_RADIUS = 5000;
    //TextView locationDetailTV;
    ExpandableListView placesDetailsListView;
    LinkedHashMap<String, ArrayList<String>> googlePlacesCollection;
    ArrayList<String> childList;
    List<String> groupList;
    GeocoderHelper geocoderHelper;
    DatabaseHandler databaseHandler;
    FloatingActionButton expandCollapseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch(Exception ex) {
            Log.e("exception", "onCreate: ",ex);
        }
        context = this;
        databaseHandler = new DatabaseHandler(this);
        geocoderHelper = new GeocoderHelper();
        //locationDetailTV = (TextView) findViewById(R.id.latlongLocation);
        placesDetailsListView= (ExpandableListView) findViewById(R.id.PlacesListView);
        expandCollapseButton = (FloatingActionButton) findViewById(R.id.addFloatingBtn);
        placesDetailsListView.setVisibility(View.GONE);
        googlePlacesCollection = new LinkedHashMap<String, ArrayList<String>>();
        createGroupList();
        createCollection();
        navigationDrawerSetup();
        floatingActionButtonSetup();
        currentLocationAndMapSetup(); //commented this method to integrate location listener code
        searchSetup();
        final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(this,groupList,googlePlacesCollection);
        placesDetailsListView.setAdapter(expListAdapter);
        setLocationListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Putting the location listener here so that the oncreate is completed
        setLocationListener();
    }

    private void navigationDrawerSetup() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        String username=LoginActivity.emailID.split("@")[0];
        userNameET = (TextView)findViewById(R.id.navBarEmailID);
        userNameET.setText(username);
        emailIDET = (TextView)findViewById(R.id.navBarUserName);
        emailIDET.setText(LoginActivity.emailID);
    }

    private void floatingActionButtonSetup() {

        expandCollapseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (placesDetailsListView.getVisibility() == View.GONE) {
                    placesDetailsListView.setVisibility(View.VISIBLE);
                } else
                    placesDetailsListView.setVisibility(View.GONE);
            }
        });

    }

    public void setLocationListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the ca se where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                //System.out.print("first login");
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET}, 10);
            }
        }
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                //locationDetailTV.setText(geocoderHelper.getCity(context, latitude, longitude));
                LatLng latLng=new LatLng(latitude,longitude);
                mapSetup(latLng);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        //min time in ms, min dist in metres
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 10, locationListener);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 10, locationListener);

                }
        }

    }

    //commenting this to integrate location listener code
   private void currentLocationAndMapSetup() {
        latitude = 42.93708;
        longitude = -75.6107;
        // TODO get current location async
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
       LatLng latLng=new LatLng(latitude,longitude);
        //locationDetailTV.setText(geocoderHelper.getCity(context, latitude, longitude));
        mapSetup(latLng);
    }

    public void mapSetup(final LatLng currentLocation){
        SupportMapFragment supportMapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);

        if(supportMapFragment != null) {
            supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mMap.setMyLocationEnabled(true);
                    mapInitialize(new LatLng(latitude, longitude));
                }
            });
        }
    }
   public void mapInitialize(LatLng currentLocation) {
       if (mMap != null) {
           mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
           mMap.addMarker(new MarkerOptions().position(currentLocation).title("You are Here"));
           mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
           searchMultipleGooglePlaces(currentLocation);
       }
   }
    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        mapInitialize(new LatLng(latitude, longitude));
    }
    private void searchSetup(){
        searchQueryET = (EditText) findViewById(R.id.placeText);
        Button btnFind = (Button) findViewById(R.id.btnFind);

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "searching", Toast.LENGTH_LONG).show();
                String type = searchQueryET.getText().toString();
                LatLng mapCenterLocation = mMap.getCameraPosition().target;
                searchGooglePlaces(mapCenterLocation, type);
            }
        });
    }

    private void searchGooglePlaces(LatLng location, String type){
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        //locationDetailTV.setText(geocoderHelper.getCity(context, location.latitude, location.longitude));
        googlePlacesUrl.append("location=" + location.latitude + "," + location.longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + type.toLowerCase());
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + Constants.GOOGLE_API_KEY);

        PlacesFetcher placesRFetcher = new PlacesFetcher(this);
        Object[] toPass = new Object[2];
        toPass[0] = mMap;
        toPass[1] = googlePlacesUrl.toString();
        placesRFetcher.execute(toPass);

    }

    private void searchMultipleGooglePlaces(LatLng location){
        ArrayList<String> types = databaseHandler.getAllCategoriesNameInDB();
        for(String type: types){
            searchGooglePlaces(location, type);
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    private void updateLocationOnMap(LatLng location, String marker){
        mMap.addMarker(new MarkerOptions().position(location).title(marker));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mLocationText.setText(" Current Location: " + marker);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_list) {
            Intent intent = new Intent(context, MyListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_add_to_do) {
            Intent intent = new Intent(context, AddToDoCategorySelectionActivity.class);
            intent.putExtra("toDoObject", new ToDo());
            startActivity(intent);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onCompleted(List<Place> placeList) {
        for(String categoryType: groupList){
            ArrayList<String> placesnames=googlePlacesCollection.get(categoryType.toLowerCase());
            if(placesnames==null){placesnames=new ArrayList<String>();}
            HashSet<String> set = new HashSet<String>(placesnames);
            for(Place place: placeList){
                if(place.types.contains(categoryType.toLowerCase())){
                    set.add(place.name);
                }
            }
            placesnames=new ArrayList<String>(set);
            googlePlacesCollection.put(categoryType.toLowerCase(), placesnames);
        }
       final ExpandableListAdapter expListAdapter=new ExpandableListAdapter(this, groupList, googlePlacesCollection);
        placesDetailsListView.setAdapter(expListAdapter);
    }

    private void createGroupList() {
        groupList = new ArrayList<String>();
        staticCategoryInitializer();
        List<ToDo> TODOArrayList = databaseHandler.getAllToDo();
        for(ToDo todo: TODOArrayList) {
            groupList.add(todo.Category.toLowerCase());
        }

    }
    private void staticCategoryInitializer(){
        ArrayList<Category> categoryArrayList = new ArrayList<Category>();
        categoryArrayList.add(new Category("1001", "ATM", "All ATM branches"));
        categoryArrayList.add(new Category("1002", "food", "All eatery"));
        categoryArrayList.add(new Category("1003", "university", "nearby Universities"));
        categoryArrayList.add(new Category("1004","hospital","All hospitals"));
        categoryArrayList.add(new Category("1005", "bus_station", "All Bus Stops"));
        categoryArrayList.add(new Category("1006", "movie_theater", "All Cinemas"));
        categoryArrayList.add(new Category("1007","pharmacy","All pharmacy stores"));
        categoryArrayList.add(new Category("1008", "gas_station", "All gas refill stations"));
        categoryArrayList.add(new Category("1009", "police", "All Police stations"));
        updateCategoryInDBAndListView(categoryArrayList);
    }

    private void updateCategoryInDBAndListView(ArrayList<Category> categoryArrayList){
        databaseHandler.insertCategory(categoryArrayList);
        List<Category> listFromDB=databaseHandler.getAllCategoriesFromDB();
    }

    private void createCollection() {
        for (String laptop : groupList) {
            ArrayList<String> placeslist = new ArrayList<String>();
            googlePlacesCollection.put(laptop.toLowerCase(), placeslist);
        }
    }

    private void setGroupIndicatorToRight() {
        /* Get the screen width */
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;

        placesDetailsListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                - getDipsFromPixel(5));
    }
    public int getDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}
