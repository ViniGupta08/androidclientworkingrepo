package com.cloudcomputing.smartmemento;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.cloudcomputing.smartmemento.DB.DatabaseHandler;
import com.cloudcomputing.smartmemento.ToDo.ToDo;
import com.cloudcomputing.smartmemento.ToDo.ToDoCustomAdapter;

import java.util.ArrayList;

public class PlacesDetailActivity extends AppCompatActivity {
    ListView myToDoListView;
    Context context;
    ArrayList<ToDo> myToDOList;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_detail);
        context = this;
        databaseHandler = new DatabaseHandler(context);
        myToDoListView = (ListView) findViewById(R.id.toDoListView);
        myToDoListView.setAdapter(new ToDoCustomAdapter(this, myToDOList));
    }
    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

    }
}
