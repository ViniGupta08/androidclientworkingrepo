package com.cloudcomputing.smartmemento.apiIntegration;

import android.os.AsyncTask;
import android.util.Log;

import com.cloudcomputing.smartmemento.googleplaces.PlacesCallBack;
import com.cloudcomputing.smartmemento.googleplaces.PlacesDisplayOnMap;
import com.cloudcomputing.smartmemento.models.Task;
import com.cloudcomputing.smartmemento.utilities.HttpFetcher;
import com.google.android.gms.maps.GoogleMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class HttpPostHelper extends AsyncTask<String, String, String> {
    String googlePlacesData = null;
    GoogleMap googleMap;
    httpPostResponseCallBack httpPostResponseCallBack;
    public HttpPostHelper(httpPostResponseCallBack httpPostResponseCallBack){
        this.httpPostResponseCallBack=httpPostResponseCallBack;
    }

    @Override
    protected String doInBackground(String... params) {
        String JsonResponse = null;
        String JsonDATA = params[1];
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        try {
            String baseUrl=params[0];
            URL url = new URL(baseUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
//set headers and method
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(JsonDATA);
// json data
            writer.close();
            InputStream inputStream = urlConnection.getInputStream();
//input stream
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String inputLine;
            while ((inputLine = reader.readLine()) != null)
                buffer.append(inputLine + "\n");
            if (buffer.length() == 0) {
                // Stream was empty. No point in parsing.
                return null;
            }
            JsonResponse = buffer.toString();

                return JsonResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {

                }
            }
        }
        return null;

    }

    @Override
    protected void onPostExecute(String result) {
        this.httpPostResponseCallBack.onCompleted(result);
    }
}