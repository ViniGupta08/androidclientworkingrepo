package com.cloudcomputing.smartmemento.apiIntegration;

import com.cloudcomputing.smartmemento.Categories.Category;

import java.util.ArrayList;

/**
 * Created by vinig on 03/11/16.
 */
public interface httpPostResponseCallBack {
    public void onCompleted(String response);
}
