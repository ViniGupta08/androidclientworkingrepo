package com.cloudcomputing.smartmemento;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.cloudcomputing.smartmemento.Categories.CategoriesCustomAdapter;
import com.cloudcomputing.smartmemento.Categories.CategoriesFetcher;
import com.cloudcomputing.smartmemento.Categories.Category;
import com.cloudcomputing.smartmemento.Categories.CategoryCallBack;
import com.cloudcomputing.smartmemento.DB.DatabaseHandler;
import com.cloudcomputing.smartmemento.ToDo.ToDo;
import com.cloudcomputing.smartmemento.utilities.Constants;

import java.util.ArrayList;
import java.util.List;


public class AddToDoCategorySelectionActivity extends AppCompatActivity implements CategoryCallBack {
    Button addToDoCategoryNextBtn;
    Context context;
    ImageView categoryFoodIV;
    ImageView categoryAtmIV;
    ImageView categoryUniversityIV;
    ImageView categoryHospitalIV;
    ImageView categoryBusStationIV;
    ImageView categoryMovietheaterIV;
    ImageView categoryPharmacyIV;
    ImageView categoryGas_StationIV;
    ImageView categoryPoliceIV;

    ListView categoryListLV;
    ToDo currentToDo;
    DatabaseHandler databaseHandler;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_do_category_selection);
        context = this;
        Intent intent = getIntent();
        currentToDo = (ToDo) intent.getSerializableExtra("toDoObject");
        databaseHandler = new DatabaseHandler(this);
        // categoryListLV = (ListView) findViewById(R.id.categoryListView);
        // CategoriesFetcher categoriesFetcher = new CategoriesFetcher(this);
        // String googleCategoriesUrl = Constants.BACKEND_URL_CATEGORY;
        //Object[] obj = new Object[1];
        //obj[0] = googleCategoriesUrl;
        //categoriesFetcher.execute(obj);
        //staticCategoryInitializer();
        categoryAtmIV = (ImageView) findViewById(R.id.categoryAtm);
        categoryAtmIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("ATM");
            }
        });

        categoryFoodIV = (ImageView) findViewById(R.id.categoryFood);
        categoryFoodIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("Food");
            }
        });
        categoryUniversityIV = (ImageView) findViewById(R.id.categoryUniversity);
        categoryUniversityIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("University");
            }
        });

        categoryHospitalIV = (ImageView) findViewById(R.id.categoryHospital);
        categoryHospitalIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("Hospital");
            }
        });
        categoryBusStationIV = (ImageView) findViewById(R.id.categorybusStation);
        categoryBusStationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("Bus_Station");
            }
        });

        categoryMovietheaterIV = (ImageView) findViewById(R.id.categoryMovietheater);
        categoryMovietheaterIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("Movie_Theater");
            }
        });
        categoryPharmacyIV = (ImageView) findViewById(R.id.categoryPharmacy);
        categoryPharmacyIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("Pharmacy");
            }
        });

        categoryGas_StationIV = (ImageView) findViewById(R.id.category_gasstation);
        categoryGas_StationIV.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToAddToDoActivity("Gas_Station");
            }
        });

        categoryPoliceIV = (ImageView) findViewById(R.id.categoryPolice);
        categoryPoliceIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddToDoActivity("Police");
            }
        });
    }

    private void staticCategoryInitializer(){
        ArrayList<Category> categoryArrayList = new ArrayList<Category>();
        categoryArrayList.add(new Category("1001", "ATM", "All ATM branches"));
        categoryArrayList.add(new Category("1002", "food", "All eatery"));
        categoryArrayList.add(new Category("1003", "university", "nearby Universities"));
        categoryArrayList.add(new Category("1004","hospital","All hospitals"));
        categoryArrayList.add(new Category("1005", "bus_station", "All Bus Stops"));
        categoryArrayList.add(new Category("1006", "movie_theater", "All Cinemas"));
        categoryArrayList.add(new Category("1007","pharmacy","All pharmacy stores"));
        categoryArrayList.add(new Category("1008", "gas_station", "All gas refill stations"));
        categoryArrayList.add(new Category("1009", "police", "All Police stations"));
        updateCategoryInDBAndListView(categoryArrayList);
    }

    private void goToAddToDoActivity(String category){
        currentToDo.Category = category;
        Intent intent = new Intent(context, AddToDoActivity.class);
        intent.putExtra("toDoObject", currentToDo);
        startActivity(intent);
        finish();
    }
    private void updateCategoryInDBAndListView(ArrayList<Category> categoryArrayList){
        databaseHandler.insertCategory(categoryArrayList);
        List<Category> listFromDB=databaseHandler.getAllCategoriesFromDB();
        CategoriesCustomAdapter categoriesCustomAdapter = new CategoriesCustomAdapter(this,listFromDB,currentToDo);
        categoryListLV.setAdapter(categoriesCustomAdapter);
    }

    @Override
    public void onCompleted(ArrayList<Category> categoryArrayList) {
        updateCategoryInDBAndListView(categoryArrayList);
    }
}
