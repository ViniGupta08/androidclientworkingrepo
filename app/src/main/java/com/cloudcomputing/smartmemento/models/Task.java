package com.cloudcomputing.smartmemento.models;

import java.util.Map;

/**
 * Created by yaminikancharana on 5/7/16.
 */
public class Task {
    public int taskId;

    public String taskName;
    public String taskDescription;

    public long taskEntry;
    public long taskDeadline;
    public long taskEnd;

    public Map<String,String> mainCategory;
    public Map<String,String> mainSubCategory;
    public String getTaskName()
    {
        return this.taskName;
    }
    public Task()
    {
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public long getTaskEntry() {
        return taskEntry;
    }

    public void setTaskEntry(long taskEntry) {
        this.taskEntry = taskEntry;
    }

    public long getTaskDeadline() {
        return taskDeadline;
    }

    public void setTaskDeadline(long taskDeadline) {
        this.taskDeadline = taskDeadline;
    }

    public long getTaskEnd() {
        return taskEnd;
    }

    public void setTaskEnd(long taskEnd) {
        this.taskEnd = taskEnd;
    }

    public Map<String, String> getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(Map<String, String> mainCategory) {
        this.mainCategory = mainCategory;
    }

    public Map<String, String> getMainSubCategory() {
        return mainSubCategory;
    }

    public void setMainSubCategory(Map<String, String> mainSubCategory) {
        this.mainSubCategory = mainSubCategory;
    }
    public String getJson(){
        String JSONData="{\n" +
                "    \"taskId\" : 30,\n" +
                "    \"taskName\" : \"Testing The task id 30\",\n" +
                "    \"taskDescription\" : \"hello task 30\",\n" +
                "    \"taskEntry\" : 12345454,\n" +
                "    \"taskDeadline\" : 13123234,\n" +
                "    \"mainCategory\": {\n" +
                "      \"categoryId\" : 3\n" +
                "    },\n" +
                "    \"mainSubCategory\" : {\n" +
                "        \"subCategoryId\" : 1\n" +
                "    }\n" +
                "}";
        return JSONData;
    }
}
