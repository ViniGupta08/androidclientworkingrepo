package com.cloudcomputing.smartmemento.Categories;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinig on 03/10/16.
 */
public class CategoriesJsonParser {
    public ArrayList<Category> parse(JSONArray jsonArray) {

        return getCategories(jsonArray);
    }

    private ArrayList<Category> getCategories(JSONArray jsonArray) {
        int categoriesCount = jsonArray.length();
        ArrayList<Category> categoryList = new ArrayList<Category>();

        for (int i = 0; i < categoriesCount; i++) {
            try {
                Category category = getCategory((JSONObject) jsonArray.get(i));
                if(category != null) {
                    categoryList.add(category);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return categoryList;
    }

    private Category getCategory(JSONObject googleCategoryJson) {
        Category category = null;

        try {
            if (!googleCategoryJson.isNull("categoryId")) {
                category = new Category();
                category.categoryId = googleCategoryJson.getString("categoryId");
            }
            if (!googleCategoryJson.isNull("categoryDescription")) {
                category.categoryDescription = googleCategoryJson.getString("categoryDescription");
            }
            if (!googleCategoryJson.isNull("categoryName")) {
                category.categoryName = googleCategoryJson.getString("categoryName");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return category;
    }
}