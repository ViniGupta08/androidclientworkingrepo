package com.cloudcomputing.smartmemento.Categories;

import java.util.ArrayList;

/**
 * Created by vinig on 03/11/16.
 */
public interface CategoryCallBack {
    public void onCompleted(ArrayList<Category> categoryArrayList);
}
