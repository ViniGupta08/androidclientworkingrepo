package com.cloudcomputing.smartmemento.Categories;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.cloudcomputing.smartmemento.googleplaces.PlacesDisplayOnMap;
import com.cloudcomputing.smartmemento.utilities.HttpFetcher;
import com.google.android.gms.maps.GoogleMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vinig on 03/10/16.
 */
public class CategoriesFetcher extends AsyncTask<Object, Integer, String>{
    String googleCategoriesData = null;
    private CategoryCallBack categoryCallBack;

    public CategoriesFetcher(CategoryCallBack categoryCallBack){
        this.categoryCallBack = categoryCallBack;
    }

    public String doInBackground(Object... inputObj) {
        try {
            HttpFetcher http = new HttpFetcher();
            Log.i("Sent categories request",(String)inputObj[0]);
            googleCategoriesData = http.read((String)inputObj[0]);
        } catch (Exception e) {
            Log.d("Google Category Read", e.toString());
        }
        return googleCategoriesData;
    }

    @Override
    protected void onPostExecute(String result) {
        CategoriesJsonParser categoriesJsonParser = new CategoriesJsonParser();
        JSONArray CategoryJson= new JSONArray();

        try {
            if(googleCategoriesData != null)
                CategoryJson = new JSONArray(googleCategoriesData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Category> categoryArrayList = categoriesJsonParser.parse(CategoryJson);
        categoryCallBack.onCompleted(categoryArrayList);
        Log.d(result,"JsonObject");
    }
}