package com.cloudcomputing.smartmemento.Categories;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cloudcomputing.smartmemento.AddToDoActivity;
import com.cloudcomputing.smartmemento.R;
import com.cloudcomputing.smartmemento.ToDo.ToDo;
import com.cloudcomputing.smartmemento.ToDoDetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinig on 03/10/16.
 */
public class CategoriesCustomAdapter extends BaseAdapter{
    List<Category> categoryArrayList;
    Context context;
    ToDo currentToDo;
    private static LayoutInflater inflater=null;

    public CategoriesCustomAdapter(Activity activity, List<Category> categoryArrayList, ToDo currentTodo){
        this.context = activity;
        this.categoryArrayList = categoryArrayList;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.currentToDo = currentTodo;

    }

    @Override
    public int getCount() {
        return categoryArrayList.size();
    }

    @Override
    public Category getItem(int position) {
        return categoryArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class Holder
    {
        TextView categoryName;
        TextView categoryDescription;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        final Category currentCategory = getItem(position);
        View rowView = inflater.inflate(R.layout.list_item_category, null);
        holder.categoryName = (TextView) rowView.findViewById(R.id.categoryName);
        holder.categoryDescription = (TextView) rowView.findViewById(R.id.categoryDescription);
        holder.categoryName.setText(currentCategory.categoryName);
        holder.categoryDescription.setText(currentCategory.categoryDescription);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentToDo.Category = currentCategory.categoryName;
                Intent intent = new Intent(context, AddToDoActivity.class);
                intent.putExtra("toDoObject", currentToDo);
                context.startActivity(intent);
                }
        });

        return rowView;
    }
}
