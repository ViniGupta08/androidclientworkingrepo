package com.cloudcomputing.smartmemento.Categories;
/**
 * Created by vinig on 03/10/16.
 */
public class Category {
    public String categoryId;
    public String categoryName;
    public String categoryDescription;
    public Category(){
        categoryId = "2";
        categoryName = "Nothing";
        categoryDescription = "Desc for nothing";
    }
    public Category(String Id, String name, String desc){
        categoryId = Id;
        categoryName = name;
        categoryDescription = desc;
    }
}
