package com.cloudcomputing.smartmemento.LoginCredentials;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by CUCS\aa2355 on 3/28/16.
 */
public interface UserApi {

    @POST("authenticate")
    Call<Response> authenticate(@Body Map<String, String> loginInfo);
}
