package com.cloudcomputing.smartmemento.LoginCredentials;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

/**
 * Created by CUCS\aa2355 on 3/28/16.
 */
public class LoginRestClient extends AsyncTask<Map,Void,Response>{

    String baseUrl = "http://ec2-54-236-86-216.compute-1.amazonaws.com/api/login/";

    @Override
    protected Response doInBackground(Map... params) {
        RetrofitRestClient client = new RetrofitRestClient(baseUrl);
        UserApi userApi = client.createService(UserApi.class);
        Map<String,String> m = new HashMap<>();
        m.put("userName","Abhishek");
        m.put("password", "Hello");
        Call<Response> call = userApi.authenticate(m);
        Response r = null;
        try {
            r = call.execute().body();
            System.out.print(r);
        } catch (IOException e) {
            // handle errors
        } catch(Exception e) {
            e.printStackTrace();
            System.out.print(e.getStackTrace());
            System.out.print(e);
        }
        return r;
    }
}
