package com.cloudcomputing.smartmemento.LoginCredentials;

/**
 * Created by vinig on 03/28/16.
 */
public class LoginInput {
    String username;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String password;
}
