package com.cloudcomputing.smartmemento.googleplaces;


import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class Place {
   public String name;
    public LatLng latLng;
    public String vicinity;
    public String reference;
    public List<String> types;
    public Place(){
        types=new ArrayList<String>();
    }
}
