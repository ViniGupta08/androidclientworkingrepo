package com.cloudcomputing.smartmemento.googleplaces;

import java.util.List;

/**
 * Created by vinig on 03/27/16.
 */
public interface PlacesCallBack {
    public void onCompleted(List<Place> placeList );
}