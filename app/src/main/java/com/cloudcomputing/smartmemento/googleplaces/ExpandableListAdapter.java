package com.cloudcomputing.smartmemento.googleplaces;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.cloudcomputing.smartmemento.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by vinig on 04/01/16.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter{
    private Activity context;
    private Map<String,ArrayList<String>> googlePlacesCollections;
    private List<String> placeList;
    public ExpandableListAdapter(Activity context, List<String> placeList, Map<String, ArrayList<String>> googlePlacesCollections){
        this.context=context;
        this.googlePlacesCollections=googlePlacesCollections;
        this.placeList=placeList;

    }

    public int getGroupCount() {
        return placeList.size();
    }


    public int getChildrenCount(int groupPosition) {
        return googlePlacesCollections.get(placeList.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return placeList.get(groupPosition);
    }


    public Object getChild(int groupPosition, int childPosition) {
        return googlePlacesCollections.get(placeList.get(groupPosition)).get(childPosition);
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    public boolean hasStableIds() {
        return true;
    }


    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String placeName =(String) getGroup(groupPosition);
        if(convertView == null){
            LayoutInflater infalflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalflater.inflate(R.layout.group_item, null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.laptop);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(placeName);
        return convertView;
    }


    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        final String laptop = (String) getChild(groupPosition,childPosition);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.child_item,null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.laptop);
        item.setText(laptop);
        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

