package com.cloudcomputing.smartmemento.googleplaces;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudcomputing.smartmemento.R;
import java.util.List;

/**
 * Created by vinig on 03/27/16.
 */
public class PlacesCustomAdapter extends BaseAdapter {
    List<Place> placesArrayList;
    Context context;
    Place place;
    private static LayoutInflater inflater=null;
    public PlacesCustomAdapter(Activity activity, List<Place> placesArrayList){
        this.context = activity;
        this.placesArrayList = placesArrayList;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return placesArrayList.size();
    }

    @Override
    public Place getItem(int position) {
        return placesArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class Holder
    {
        TextView placeName;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        final Place currentPlace = getItem(position);
        View rowView = inflater.inflate(R.layout.list_item_place, null);
        holder.placeName = (TextView) rowView.findViewById(R.id.listItemPlace_PlaceName);
        holder.placeName.setText(currentPlace.name);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(this.context, currentPlace.name, Toast.LENGTH_LONG);
                Toast.makeText(context,currentPlace.name,Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }
}



